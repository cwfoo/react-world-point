'use strict';
const React = require('react');
const d3Geo = require('d3-geo');
const geojson = require('./world-countries.geo.json');  // JSON data.

class WorldMap extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {
            mapWidth,
            mapHeight,
            lat,
            lon,
        } = this.props;

        const projection = d3Geo.geoMercator().fitSize([mapWidth, mapHeight], geojson);
        const path = d3Geo.geoPath().projection(projection);

        const countries = geojson.features.map((d,i) => {
            return <path key={'path' + i} d={path(d)} />;
        });

        // Convert the lat/long coordinates into coordinates on the SVG map.
        const svgCoordinates = projection([lon, lat]);
        const cx = svgCoordinates[0],
              cy = svgCoordinates[1];

        return(
            <svg width={mapWidth} height={mapHeight}>
                {/* Draw a border around the map. */}
                <rect x="0" y="0" width={mapWidth} height={mapHeight} stroke="black" strokeWidth="1" fill="none"></rect>
                {/* Draw map. */}
                {countries}
                {/* Draw the point. */}
                <circle cx={cx} cy={cy} r="5px" fill="red"></circle>
            </svg>
        )
    }
}

module.exports = {WorldMap};

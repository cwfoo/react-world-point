const path = require('path');

module.exports = {
    mode: 'production',
    entry: {
        'world-map': './src/world-map.jsx',
    },
    output: {
        path: path.resolve(__dirname, '_build'),
        filename: '[name].js',
        libraryTarget: 'commonjs2',  // So that the output is an exportable module.
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /(_build|node_modules)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: [
                            '@babel/env',
                            '@babel/react',
                        ]
                    }
                }
            },
        ]
    },
    externals: {
        // Use the react dependency of the parent project instead of using the
        // react dependency of this project.
        'react': 'commonjs react',
    }
};

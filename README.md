# React World Point

A React component that shows a point on a world map.

```bash
npm install --save react-world-point
```

```jsx
// Tokyo, Japan.
<WorldMap lat={35.68} lon={139.68} mapWidth={600} mapHeight={500} />
```

![Location of Tokyo](screenshot.png)

## Usage

```jsx
const {WorldMap} = require('react-world-point');
```

```jsx
<WorldMap lat={lat} lon={lon} mapWidth={mapWidth} mapHeight={mapHeight} />
```

* `lat`: Decimal latitude of the point to be shown on the world map.
* `lon`: Decimal longitude of the point to be shown on the world map.
* `mapWidth`: Width of the world map in pixels.
* `mapHeight`: Height of the world map in pixels.

## Development

To build:

```bash
npm install
npm run build
```

The resulting files will be in the `_build/` directory.

## License

This project is distributed under the BSD 3-Clause License (see LICENSE).
This project uses third-party libraries that are licensed under their own terms
(see LICENSE-3RD-PARTY).
